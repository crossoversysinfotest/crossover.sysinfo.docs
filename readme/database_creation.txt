# Database Creation

After MySql Server running, configure the user and password in:
  source/crossover.sysinfo.scripts/sql/create_db.sh

Then execute the create_db.sh script. 
All data base will be created.

Alternativelly, you can create the database via PhpMyAdmin.
After accessing the page, run the sql commands that are inside the file:
  source/crossover.sysinfo.scripts/sql/create_db_and_tables.sql


