# Table of Contents

 - Intro
 - Managing the project
 - Download
 - Dependencies
 - Database creation
 - Build
 - Deploy
 - Running
 - Estimative
 - Requirements not covered
 - Issues
 - Feedback

# Introduction

This project were developed totally under Linux Ubuntu 16.04.

All sources files, drafts, thoughts and informations are inside "source" folder as:

source/
├── crossover.sysinfo.client
├── crossover.sysinfo.docs
├── crossover.sysinfo.scripts
├── crossover.sysinfo.server
├── eclipse_workspace
├── trd.lib.cppzmq
├── trd.lib.gtest
└── trd.lib.zmq

All folders are git repostories in bitbucket, and they have a lot of commits that were done during the development phase.
The repositories are public and can be accessed via:
https://bitbucket.org/crossoversysinfotest/trd.lib.zmq.git
https://bitbucket.org/crossoversysinfotest/trd.lib.cppzmq.git
https://bitbucket.org/crossoversysinfotest/trd.lib.gtest.git
https://bitbucket.org/crossoversysinfotest/crossover.sysinfo.server.git
https://bitbucket.org/crossoversysinfotest/crossover.sysinfo.client.git
https://bitbucket.org/crossoversysinfotest/crossover.sysinfo.docs.git
https://bitbucket.org/crossoversysinfotest/crossover.sysinfo.scripts.git

To easily download the repos, you can download only scripts folder, and run "clone.sh" script.

## crossover.sysinfo.docs
source/crossover.sysinfo.docs/
├── Assignment.pdf
├── design
├── images
├── others
├── readme
└── spreadsheets

The "design" folder contains the original files for the Design.pdf document.
The "images" folder contains all the images used in Design.pdf document, and also the public link of draw.io document where the images were created.
The "spreadsheets" folder contais the work estimative sheet.
The "readme" fodler contais all splitted parts of readme file.
The "othes" folder contais my thoughts during the development, like project understanding, activities breakdown, emails drafts, etc. 

## crossover.sysinfo.scripts
This git repository stores all developed scripts:
 - clone.sh, a script that can download others repositories
 - build.sh, a script that can build(compile) all repositories
 - sandbox folder, a folder that have the scripts I used for testing the third parties libraries
 - sql, a folder that contains a script to create database and tables in MySQL

## crossover.sysinfo.client
The client application repository

## crossover.sysinfo.server
The server application repository

## trd.lib.cppzmq
The third library CPPZmq. This is the header for zmq.hpp.

## trd.lib.zmq 
The third library ZeroMQ.

## trd.lib.gtest
The third library GoogleTest.

# Managing the project

As per my experience, when you user some development methodology, yuo get better results. So I decide to use scrum, by adding this project to Jira.
So, this project were managed totally via Attalassian Jira.

After the initial breakdown, the following activities were mapped into jira issues with the estimative of work time:

4h Understanding the project with Stakeholders and defining activities (already executed)
1h Open all jiras in BitBucket+Jira Atlassian for management tracking  (will not create sprints)
2h High level design of the solution
2h Fast study Mqtt and decide if fits to this project. If not, check if protocol buffer and simple sockets will fit. If not, then, search on internet for a "ready to use" protocol mechanism.
1h Definition of all requireds git repositories
1h Creation of all requireds git repositories in Bitbucket
2h Clone of all requireds git repositories inside sources folder (Create a script for that or use any "read-to-use" third party)
1h Creation of a script to build eclipse workspace
3h Prepare build script to build all repositories inside sources folder (using make)
2h Create targets - Each repository must have a Debug target and a Debug-Simulation target (fot testing purpouse)
1h Build script must get the TAG from GIT for each repository and create a file "version.h" with this TAG
3h Model the database tables
1h Create scripts to "create" the database and all required tables
4h Server app - Low level design and document writing
4h Client app - Low level design and document writing
1h Server app - definition of classes
3h Server app - definition of Unit/Integration Tests
16h Server app - code and unit test implementation (to be detailed)
1h Client app - definition of classes
2h Client app - definition of Unit/Integration Tests
16h Client app - code and unit test implemenatation (to be detailed)
2h Integration tests between Server and Client app
4h Bug fixes 
1h Prepare the "Description section" of Readme file
1h Prepare the "Dependencies section" of Readme file
1h Prepare the "Building section" of Readme file
1h Prepare the "Install section" of Readme file
2h Prepare the "Running section" of Readme file
3h Prepare gcov script to scan all source files
1h Prepare the "Checking the test coverage section" of Readme file
1h Prepare the "Issues faced section" of Readme file
1h Prepare the "Feedback section" of Readme file
1h Record movie of running system
6h Web app - present data in beautiful tables (only if time is sufficient)
Total 96  h

All activities can be found in Jira in the following link:
https://danilopagano.atlassian.net/projects/CROS/

If you don't have access, try with this user:
email: contato@danilopagano.com
pass: crossover10

I created this user with administrator privelegies, so you are able to see every move.



# Download

After downloading and extrating the ".zip' file, you won't have third parties source files, because they are too big.
So, to download them, execute:

cd source/crossover.sysinfo.scripts
./clone

The repositories that are already present will be skipped.
# Dependencies:

The following dependencies need to be resolved before building: 
 - Install MySQL server
    To achieve this, please take a look at: https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04
 - Download, Build and install ZMQ
    After reading Download section, please execute:
      sudo apt-get install libtool
      cd source/trd.lib.zmq/
      ./crossover_build.sh
      sudo make install

 - Download, Build and install CPPZMQ
    After reading Download section, please execute:
      cd source/trd.lib.cppzmq/
      ./crossover_build.sh
      sudo make install

After this, all dependencies must be resolved.
# Database Creation

After MySql Server running, configure the user and password in:
  source/crossover.sysinfo.scripts/sql/create_db.sh

Then execute the create_db.sh script. 
All data base will be created.

Alternativelly, you can create the database via PhpMyAdmin.
After accessing the page, run the sql commands that are inside the file:
  source/crossover.sysinfo.scripts/sql/create_db_and_tables.sql


# Building 

It is possible to build all repositories at once, or build each of them uniquely. 
To build all, execute:

cd source/crossover.sysinfo.scripts/
./build.sh

To build each repository manually, execute:

cd source/repositoryname/
./crossover_build.sh

Examples:
Build server application:

cd source/crossover.sysinfo.server/
./crossover_build.sh

Build client application:

cd source/crossover.sysinfo.client/
./crossover_build.sh




# Deploy 

Only Server and Client application (with respectively configuration file) can be deployed.

Server:
cd source/crossover.sysinfo.server/
./crossover_deploy.sh

Client:
cd source/crossover.sysinfo.client/
./crossover_deploy.sh

The executable and configurations file will be copied to "deploy" folder, in the same level as source folder:
.
├── demo
├── deploy
├── Design.pdf
└── source

The expect result is:

deploy/
├── client
│   ├── client
│   └── client.conf
└── server
    ├── clients_config.xml
    ├── mysql.conf
    ├── server
    ├── server.conf
    └── smtp.conf


# Running

To run the client:
cd deploy/client
./client

To run the server:
cd deploy/server
./server

The deploy/client/client.conf file determines some important information:
 id 10001 ---> the same as in database
 port 5556 ---> default port for the client
 timeout 5 ---> timeout in seconds. 

The server should configure:
 deploy/server/smtp.conf -> configurations for sending email
 deploy/server/mysql.conf -> configurations for mysql connection
 deploy/server/clients_config.xml -> configurations for configuring the connection to the clients



# Requeriments not covered

There are many non covered requiriements:
 - server store data in Database
 - server sending email if alert limit achieved
 - server connecting to multiple clients
 - unit test for everything
 - video for full system running
 
The server application for now is only a concept proof
All this is not covered because my time to implement is very limited.


# Feedback

This project is very good for demonstrating the knowledge in many technologies.
I really enjoyed doing this.

On the other hand, for a quality work, it requires more then 3 days to implement. 
As you can see, in my estimative, I would take around 100 hours to do a real quality work.

Unfortunnatelly, my time is very limited, and I worked:
12 hours on friday 27/10
4 hours on saturday 28/10
5 hours on sunday 29/10

Also, as I really like a very well structured work, I spend some time in organizing the job, which let me even with less time to really work on the code.


