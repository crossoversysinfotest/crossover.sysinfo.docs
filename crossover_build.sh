#!/bin/bash

OUTFILE="README.txt"

cd readme
rm ${OUTFILE}
cat table_of_contents.txt >> ${OUTFILE}
cat intro.txt             >> ${OUTFILE}
cat managing.txt          >> ${OUTFILE}
cat download.txt          >> ${OUTFILE}
cat dependencies.txt      >> ${OUTFILE}
cat database_creation.txt >> ${OUTFILE}
cat build.txt             >> ${OUTFILE}
cat deploy.txt            >> ${OUTFILE}
cat running.txt           >> ${OUTFILE}
cat requirements_not_covered.txt >> ${OUTFILE}
cat feedback.txt          >> ${OUTFILE}
